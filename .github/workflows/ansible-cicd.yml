---
name: Ansible CI/CD
on:
  pull_request:
    types:
      - opened
      - synchronize
      - reopened
      - closed
    branches:
      - main
    paths:
      - "ansible/host_vars/**"
  workflow_dispatch:
    inputs:
      host:
        description: "Host to run Ansible against"
        required: true
        type: choice
        options:
          - "allaboutsecurity"
          - "code"
          - "firefly"
          - "gaming"
          - "gaps"
          - "healthchecks"
          - "innernet"
          - "monitoring"
          - "nextcloud"
          - "nodered"
          - "pihole"
          - "securemylife"
          - "timemachine"
          - "vault"
          - "youtubedl"
      dry_run:
        description: "Dry run"
        required: true
        type: boolean
        default: true
jobs:
  setup:
    runs-on: ubuntu-latest
    outputs:
      host: ${{ steps.set-host.outputs.host }}
      dry_run: ${{ steps.set-dry-run.outputs.dry_run }}
    steps:
      - name: Checkout (pull_request)
        uses: actions/checkout@c85c95e3d7251135ab7dc9ce3241c5835cc595a9 # v3
        if: github.event_name == 'pull_request'
        with:
          ref: ${{ github.event.pull_request.head.sha }}
      - name: Checkout (workflow_dispatch || push)
        uses: actions/checkout@c85c95e3d7251135ab7dc9ce3241c5835cc595a9 # v3
        if: github.event_name != 'pull_request'
      - name: Fetch main branch
        run: git fetch origin main
      - name: Set host
        id: set-host
        run: |
          if [ "${{ github.event_name }}" == "workflow_dispatch" ]; then
            echo "host=${{ github.event.inputs.host }}" >> $GITHUB_OUTPUT
          elif [ "${{ github.event_name }}" == "pull_request" ]; then
            # Get list of subdirectories in ansible/host_vars that have changed vs PR base SHA
            CHANGED_HOSTS=$(git diff --name-only ${{ github.event.pull_request.base.sha }} | grep -oP 'ansible/host_vars/\K[^/]+')
            # Set host to list of changed hosts, using , as a delimiter
            echo "host=$(echo $CHANGED_HOSTS | tr ' ' ',')" >> $GITHUB_OUTPUT
          else
            echo "Unknown event name: ${{ github.event_name }}"
            exit 1
          fi
      - name: Set dry run (workflow_dispatch)
        id: set-dry-run
        run: |
          if [ "${{ github.event_name }}" == "workflow_dispatch" ]; then
            echo "dry_run=${{ github.event.inputs.dry_run }}" >> $GITHUB_OUTPUT
          elif [ "${{ github.event_name }}" == "pull_request" ]; then
            if [ "${{ github.event.pull_request.merged }}" == "true" ]; then
              echo "dry_run=false" >> $GITHUB_OUTPUT
            else
              echo "dry_run=true" >> $GITHUB_OUTPUT
            fi
          else
            echo "Unknown event name: ${{ github.event_name }}"
            exit 1
          fi
      - name: Summary
        run: |
          echo "# Inputs Summary" >> $GITHUB_STEP_SUMMARY
          echo "| Input | Value |" >> $GITHUB_STEP_SUMMARY
          echo "| --- | --- |" >> $GITHUB_STEP_SUMMARY
          echo "| host | ${{ steps.set-host.outputs.host }} |" >> $GITHUB_STEP_SUMMARY
          echo "| dry_run | ${{ steps.set-dry-run.outputs.dry_run }} |" >> $GITHUB_STEP_SUMMARY

  ansible:
    runs-on: ubuntu-latest
    needs: setup
    env:
      ANSIBLE_ROLES_PATH: ${{ github.workspace }}/ansible/roles
      ANSIBLE_COLLECTIONS_PATH: ${{ github.workspace }}/ansible/collections
    steps:
      - name: Checkout
        uses: actions/checkout@c85c95e3d7251135ab7dc9ce3241c5835cc595a9 # v3
      - name: Setup Tailscale
        uses: tailscale/github-action@65cdd9a05d7ebe4ef4e8c70141f5d84e1cd4cab4 # v2
        with:
          version: 1.34.1
          authkey: ${{ secrets.TAILSCALE_AUTHKEY }}
      - name: Disallow DNS configuration from Tailscale admin panel
        run: sudo tailscale set --accept-dns=false
      - name: Setup Taskfile
        uses: arduino/setup-task@v1
        with:
          repo-token: ${{ secrets.GITHUB_TOKEN }}
      - name: Setup Python
        uses: actions/setup-python@bd6b4b6205c4dbad673328db7b31b7fab9e241c0 # v4
        with:
          python-version: 3.9
          cache: pip
      - name: Cache Ansible Roles
        uses: actions/cache@88522ab9f39a2ea568f7027eddc7d8d8bc9d59c8 # v3
        with:
          path: ${{ env.ANSIBLE_ROLES_PATH }}
          key: ${{ runner.os }}-ansible-roles-${{ hashFiles('ansible/requirements.yml') }}
      - name: Cache Ansible Collections
        uses: actions/cache@88522ab9f39a2ea568f7027eddc7d8d8bc9d59c8 # v3
        with:
          path: ${{ env.ANSIBLE_COLLECTIONS_PATH }}
          key: ${{ runner.os }}-ansible-collections-${{ hashFiles('ansible/requirements.yml') }}
      - name: Setup CICD
        run: task setup-cicd
      # Loop through hosts
      - name: Run Ansible
        env:
          ANSIBLE_VAULT_PASSWORD: ${{ secrets.ANSIBLE_VAULT_PASSWORD }}
          GH_TOKEN: ${{ secrets.GITHUB_TOKEN }}
        run: |
          echo "# Ansible CICD Summary" >> $GITHUB_STEP_SUMMARY
          echo "Job: [${{ github.job }}]($GITHUB_SERVER_URL/$GITHUB_REPOSITORY/actions/runs/${{ github.run_id }})" >> $GITHUB_STEP_SUMMARY
          echo "| Host | Status |" >> $GITHUB_STEP_SUMMARY
          echo "| --- | --- |" >> $GITHUB_STEP_SUMMARY

          FINAL_STATUS=0

          # Create list of hosts to run Ansible for, splitting on ,
          HOSTS=$(echo ${{ needs.setup.outputs.host }} | tr ',' ' ')

          for host in $HOSTS; do
            STATUS_EMOJI=""

            if ! ansible --list-hosts all | grep -q $host; then
              echo "$host is not an available host, skipping"
              continue
            fi

            echo ::group::Pinging $host
            HOST=$host task ansible-ping
            STATUS_PING=$?
            echo ::endgroup::

            if [ $STATUS_PING -ne 0 ]; then
              STATUS_EMOJI=":warning:"
              echo "$host is unreachable, skipping"
            else
              echo ::group::Running Ansible for $host
              HOST=$host DRY_RUN=${{ needs.setup.outputs.dry_run }} task ansible
              STATUS=$?
              echo ::endgroup::

              if [ $STATUS -ne 0 ]; then
                FINAL_STATUS=$STATUS
              fi

              if [ $STATUS -eq 0 ]; then
                STATUS_EMOJI=":white_check_mark:"
              else
                STATUS_EMOJI=":x:"
              fi
            fi

            echo "| $host | $STATUS_EMOJI |" >> $GITHUB_STEP_SUMMARY
          done

          if [ "${{ github.event_name }}" == "pull_request" ]; then
            gh pr comment ${{ github.event.pull_request.number }} --body "$(cat $GITHUB_STEP_SUMMARY)"
          fi

          exit $FINAL_STATUS
      - name: Tailscale Logout
        if: always()
        run: sudo tailscale logout
