---
- name: Include variables
  ansible.builtin.include_vars: vault_main.yml

- name: Check if cloudflared is installed
  become: true
  ansible.builtin.command: dpkg-query -W cloudflared
  register: cloudflared_install_check
  failed_when: cloudflared_install_check.rc > 1
  changed_when: cloudflared_install_check.rc == 1

- name: Install cloudflared
  when: cloudflared_install_check.rc == 1
  ansible.builtin.include_tasks: install.yml

- name: Check if cloudflared has initial setup
  become: true
  ansible.builtin.stat:
    path: /etc/cloudflared/config.yml
  register: cloudflared_setup_check

- name: Setup cloudflared
  when: not cloudflared_setup_check.stat.exists
  ansible.builtin.include_tasks: setup.yml

- name: Get required cloudflared values
  become: true
  ansible.builtin.shell: cat /root/.cloudflared/config.yml | head -n 1 | awk '{print $2}'
  register: tunnel_output

- name: Debug additional_ingress
  ansible.builtin.debug:
    var: additional_ingress

- name: Create updated cloudflared config file
  become: true
  vars:
    cf_tunnel_id: "{{ tunnel_output.stdout }}"
  ansible.builtin.template:
    src: ../templates/config.yml.j2
    dest: /tmp/cloudflared-config.yml

- name: Validate updated config ingress rules
  become: true
  ansible.builtin.command: cloudflared --config /tmp/cloudflared-config.yml --no-autoupdate tunnel ingress validate
  register: validation_check

- name: Diff updated config with current cloudflared config
  become: true
  ansible.builtin.command: diff /tmp/cloudflared-config.yml /etc/cloudflared/config.yml
  ignore_errors: true
  when: validation_check.rc == 0
  register: diff_result

- name: Copy updated cloudflared config file to /etc/cloudflared
  become: true
  when: validation_check.rc == 0 and diff_result.rc != 0
  ansible.builtin.command:
    cmd: cp /tmp/cloudflared-config.yml /etc/cloudflared/config.yml

- name: Restart cloudflared service
  become: true
  ansible.builtin.systemd:
    name: cloudflared
    state: restarted
    no_block: true
  when: validation_check.rc == 0 and diff_result.rc != 0
  async: 5
  poll: 1

- name: Reset connection
  ansible.builtin.meta: reset_connection

- name: Wait for the cloudflared service to reload
  when: diff_result.rc != 0 and validation_check.rc == 0
  ansible.builtin.wait_for_connection:
    delay: 10
