oci==2.105.0
dnspython==2.3.0
jmespath==1.0.1
cryptography>=39.0.1 # not directly required, pinned by Snyk to avoid a vulnerability
